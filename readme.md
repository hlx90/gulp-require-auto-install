# Getting Started
This tool can be installed as a global npm package.        

```
sudo npm install -g gulp-require-auto-install
```

Use`cd /path/to/gulp/dir`go to the directory which need install gulp's requirements,and use the command bellow:   

```
gulp-require-auto-install
```

# Usage

```
gulp-require-auto-install --help
```

```
Usage: gulp-require-auto-install [options]

  Options:

    -h, --help                output usage information
    -V, --version             output the version number
    -g, --gulp-file [path]    Set gulpfile.js
    -p, --package [path]      Set package.json
    -n, --npm-exc [executor]  Set npm executor
    -d, --dev                 Save to devDependencies  
```

# Suggestion
Add `alias gri='gulp-require-auto-install'` to your shell-rc file.   
在某些网络环境情况下，使用`gulp-require-auto-install -n cnpm`会比较合适。