var fs = require('fs');
var analysisRequire = require('./analysis-require');

//cat gulpfile.js | node ~/path/to/gulp-require-auto-install
process.stdin.setEncoding('utf8');
var stdinData = "";
process.stdin.on('readable', () => {
    var chunk = process.stdin.read();
    if (chunk !== null) {
        //  process.stdout.write(`data: ${chunk}`);
        stdinData += chunk;
    }
});

process.stdin.on('end', () => {
    //  process.stdout.write('end');
    var res = analysisRequire(stdinData);
    process.stdout.write(res + "\n");
});
