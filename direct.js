var fs = require('fs');
var analysisRequire = require('./analysis-require');

var env = process.env
    // console.log(env);

var pwd = env.PWD;

var gulpfile = fs.readFileSync(pwd + '/gulpfile.js', 'utf-8');
var pkg = fs.readFileSync(pwd + '/package.json', 'utf-8');
pkg = JSON.parse(pkg);
var installed = pkg.dependencies;

var need = analysisRequire(gulpfile);
need = need.split(' ');
var res = [];
for (item of need) {
    if (item in installed) {
        continue;
    }
    res.push(item);
}
res = res.join(' ');
console.log(res);
