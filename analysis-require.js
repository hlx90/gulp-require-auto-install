var analysisRequire = function(content) {
    var res = '';
    var requireReg = /require\(('|")(?!\.\/)([^"']+)\1\)/g;
    var reqMatchs = content.match(requireReg);
    reqMatchs = reqMatchs.join(' ');
    reqMatchs = reqMatchs.replace(requireReg, '$2');
    res += reqMatchs;
    var loaderReg = /\$\.([^(), .]+)/g;
    var reqMatchs = content.match(loaderReg);
    reqMatchs = Array.from(new Set(reqMatchs)).join(' ');
    reqMatchs = reqMatchs.replace(loaderReg, 'gulp-$1');
    reqMatchs = reqMatchs.replace(/([A-Z])/g, "-$1").toLowerCase();
    res += ' ' + reqMatchs;
    res = Array.from(new Set(res.split(' '))).join(' ');
    return res;
}
module.exports = analysisRequire;
