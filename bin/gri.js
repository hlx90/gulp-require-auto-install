#!/usr/bin/env node

/**
 * Module dependencies.
 */

var program = require('commander');
var fs = require('fs');
var analysisRequire = require('../analysis-require');

program
    .version(require('../package').version)
    .option('-g, --gulp-file [path]', 'Set gulpfile.js')
    .option('-p, --package [path]', 'Set package.json')
    .option('-n, --npm-exc [executor]', 'Set npm executor')
    .option('-d, --dev', 'Save to devDependencies')
    .parse(process.argv);

// console.log(program);

// console.log('you ordered a pizza with:');
var gulpfilePath = program.gulpFile || 'gulpfile.js';
var packagePath = program.package || 'package.json';
var npmExc = program.npmExc || 'npm';
var dev = program.dev;
var saveMode = dev ? '--save-dev' : '--save';
// if (program.gulpFile) console.log('  - gulpFile', program.gulpFile);
// if (program.package) console.log('  - package', program.package);
// console.log(gulpfilePath, packagePath, npmExc);

var env = process.env
    // console.log(env);

var pwd = env.PWD;

var gulpfile = fs.readFileSync(gulpfilePath, 'utf-8');
var pkg = fs.readFileSync(packagePath, 'utf-8');
pkg = JSON.parse(pkg);
var installed = pkg.dependencies || {};

for (var key in pkg.devDependencies || {}) {
    installed[key] = pkg.devDependencies[key];
}
// console.log(installed);

var need = analysisRequire(gulpfile);
need = need.split(' ');
var res = [];
for (item of need) {
    if (item in installed) {
        continue;
    }
    res.push(item);
}
res = res.join(' ');
console.log(res);
const exec = require('child_process').exec;
var cmd = npmExc + ' install ' + saveMode + ' ' + res;
// cmd = 'cat ' + packagePath;
exec(cmd, {
    cwd: pwd
}, (error, stdout, stderr) => {
    if (error) {
        console.error(`exec error: ${error}`);
        return;
    }
    console.log(`stdout: \n${stdout}`);
    console.log(`stderr: \n${stderr}`);
});
